let delimetersVar1 = ['\t', 'CA WP', '>', 'CH-NO', 'CHNO', '\n', ', CA, WP'];
let delimetersVar2 = ['  '];
let delimetersVar3 = ['...-', '..-'];

function clearBreakALine () {
    idInputTextBreakALine.value = '';
    idDelimetersForBreakText.value = '';    
    idRemoveForBreakText.value = '';
}

function removeSymbols () {
    if(!idRemoveForBreakText.value.length) {
        let input = idInputTextBreakALine.value;
        idInputTextBreakALine.value = privateRemoveSymbols(input);
    } else {
        //debugger;
        let input = idInputTextBreakALine.value;
        let key = idRemoveForBreakText.value;
        
        idInputTextBreakALine.value = privateRemoveSymbols(input, key);
    }

    function privateRemoveSymbols(input = '', key = '') {
        input = input.trim().toUpperCase();
        if(key !== ' ') {
            key = key.trim().toUpperCase();
        }
        
        if(key !== '') {
            while(input.indexOf(key) !== -1) {
                input = input.replace(key, '');
            }
        }

        for(let i = 0; i < delimetersVar1.length; i++){
            while(input.indexOf(delimetersVar1[i]) !== -1) {
                input = input.replace(delimetersVar1[i], '');
            }
        }
        
        for(let i = 0; i < delimetersVar2.length; i++){
            while(input.indexOf(delimetersVar2[i]) !== -1) {
                input = input.replace(delimetersVar2[i], ' ');
            }
        }

        for(let i = 0; i < delimetersVar3.length; i++){
            while(input.indexOf(delimetersVar3[i]) !== -1) {
                if(input.indexOf(delimetersVar3[i]) === 0) {
                    input = input.replace(delimetersVar3[i], '');    
                } else {
                    input = input.replace(delimetersVar3[i], ', ');
                }
            }
        }

        return input;
    }
}